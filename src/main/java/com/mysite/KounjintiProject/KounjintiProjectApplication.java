package com.mysite.KounjintiProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KounjintiProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(KounjintiProjectApplication.class, args);
	}

}
