package com.mysite.KounjintiProject.model;

import java.util.List;

public class Store {

    public Integer id;
    public String address;
    public String name;
    public List<Ingredient> ingredients;
}
