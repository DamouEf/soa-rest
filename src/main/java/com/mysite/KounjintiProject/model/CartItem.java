package com.mysite.KounjintiProject.model;

public class CartItem {

    public Integer id;
    public Ingredient ingredientId;
    public Float subTotal;
    public Float quantity;
}
