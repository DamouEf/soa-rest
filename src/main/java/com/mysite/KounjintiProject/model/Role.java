package com.mysite.KounjintiProject.model;

public enum Role {
    ADMIN, AUTHOR, SIMPLE_USER
}
