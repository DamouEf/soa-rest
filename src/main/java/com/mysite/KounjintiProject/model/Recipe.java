package com.mysite.KounjintiProject.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="Recipe")
public class Recipe implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;
    public String name;
    public String time;
    public String description;

    public Recipe() {
    }

    public Recipe(int id, String name, String time, String description) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    /*@JsonManagedReference(value="ingredients")
    @OneToMany(mappedBy="IngredientId",fetch=FetchType.LAZY,cascade=CascadeType.REMOVE)
    private List<Recipe_Ingredient> ingredients;

    public List<Recipe_Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Recipe_Ingredient> ingredients) {
        this.ingredients = ingredients;
    }*/
}
