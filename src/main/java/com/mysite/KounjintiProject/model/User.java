package com.mysite.KounjintiProject.model;

public class User {
    public Integer id;
    public String name;
    public String lastname;
    public String username;
    public Role position;
}
