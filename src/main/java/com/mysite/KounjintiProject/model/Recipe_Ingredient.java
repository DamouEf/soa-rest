package com.mysite.KounjintiProject.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Recipe_Ingredient")
public class Recipe_Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @ManyToOne(optional = false)
    public Ingredient ingredientId;

    @ManyToOne(optional = false)
    public Recipe recipeId;

    public Float quantity;

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }
}
